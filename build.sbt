name := """scala-template"""

val mainClassName = "template.Main"

// organization := com.mycompany

version := "0.1"

scalaVersion := "2.11.7"

mainClass in Compile := Some(mainClassName)

libraryDependencies ++= Seq(
  // scala-logging
  "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2",
  "ch.qos.logback" % "logback-classic" % "1.0.13",
  // scalatest
  "org.scalatest" %% "scalatest" % "2.2.4" % "test"

  // specs 2
  /*
  "org.specs2" %% "specs2-analysis" % "2.4.16" % "test",
  "org.specs2" %% "specs2-common" % "2.4.16" % "test",
  "org.specs2" %% "specs2-core" % "2.4.16" % "test",
  "org.specs2" %% "specs2-junit" % "2.4.16" % "test",
  "org.specs2" %% "specs2-matcher-extra" % "2.4.16" % "test",
  "org.specs2" %% "specs2-matcher" % "2.4.16" % "test",
  "org.specs2" %% "specs2-mock" % "2.4.16" % "test"
  */
)

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  "-Ywarn-adapted-args",
  "-Xlint",
  "-encoding", "UTF-8"
)

assemblyJarName in assembly := {
  name.value + "-" + version.value + "-assembly.jar"
}

mainClass in assembly := Some(mainClassName)

test in assembly := {}
