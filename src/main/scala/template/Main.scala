package template

import com.typesafe.scalalogging.slf4j.LazyLogging

object Main extends LazyLogging {

  def main(args: Array[String]): Unit = {
    println("Hello World!")
    logger.info("Hello World!")
  }

}