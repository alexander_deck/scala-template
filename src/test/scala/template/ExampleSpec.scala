package template

import org.scalatest._

import scala.collection.mutable


class ExampleSpec extends FlatSpec with Matchers {

  "A Stack" should "pop values in last-in-first-out order" in {
    val stack = new mutable.Stack[Int]
    stack.push(1)
    stack.push(2)
    stack.pop() shouldBe 2
    stack.pop() shouldBe 1
  }

  it should "throw NoSuchElementException if an empty stack is popped" in {
    val emptyStack = new mutable.Stack[Int]
    a[NoSuchElementException] shouldBe thrownBy {
      emptyStack.pop()
    }
  }
}